## Aufgabe 1

# (a)

Σ<sup>0</sup> = {Ø}  
Σ<sup>2</sup> = {aa, ab, ac, ba, bb, bc, ca, cb, cc}  

Σ<sup>0</sup> ∪ Σ<sup>2</sup> =  
&nbsp;&nbsp;&nbsp;&nbsp;**{Ø, aa, ab, ac, ba, bb, bc, ca, cb, cc}**  

# (b)

Σ<sup>3</sup> = {  
&nbsp;&nbsp;&nbsp;&nbsp;000, 001, 010, 011, 100, 101, 110, 111  
}

Σ<sup>4</sup> = {  
&nbsp;&nbsp;&nbsp;&nbsp;0000, 0001, 0010, 0011, 0100, 0101, 0110, 0111,  
&nbsp;&nbsp;&nbsp;&nbsp;1000, 1001, 1010, 1011, 1100, 1101, 1110, 1111  
}  

Σ<sup>3</sup> | w = w<sup>R</sup> = {000, 010, 101, 111}

Σ<sup>4</sup> | w = w<sup>R</sup> = {0000, 0110, 1001, 1111}

L =  
&nbsp;&nbsp;&nbsp;&nbsp;{ w | w ∈ Σ<sup>3</sup> ∪ Σ<sup>4</sup>, w = w<sup>R</sup> } =  
&nbsp;&nbsp;&nbsp;&nbsp;**{000, 010, 101, 111, 0000, 0110, 1001, 1111}**  

# (c)

## i.  
**n<sup>m</sup>**  

## ii.
TODO

---

## Aufgabe 2

![Antwort](./ESA1_Loesung2.png)

## Aufgabe 3

b)

![Antwort](./ESA1_Loesung3b1.png)

| | |a|b| |
|-|-|-|-|-|
|q<sub>0</sub>| {z<sub>2</sub>, z<sub>3</sub>, z<sub>1</sub>} | {z<sub>2</sub>, z<sub>3</sub>, z<sub>1</sub>} | {z<sub>3</sub>, z<sub>1</sub>} | valid weil z<sub>2</sub> enthalten
|q<sub>1</sub>| {z<sub>3</sub>, z<sub>1</sub>} | {z<sub>2</sub>, z<sub>3</sub>, z<sub>1</sub>} | {z<sub>3</sub>, z<sub>1</sub>} |

![Antwort](./ESA1_Loesung3b2.png)

## Aufgabe 4

a)

![Antwort](./ESA1_Aufgabe4a.png)

b)

|  |          |a         |b         |       |
|- |-         |-         |-         |-      |
|q<sub>0</sub>| {z<sub>1</sub>}     | {z<sub>1</sub>, z<sub>2</sub>} | {z<sub>2</sub>, z<sub>1</sub>} | valid |
|q<sub>1</sub>| {z<sub>1</sub>, z<sub>2</sub>} | {z<sub>1</sub>, z<sub>2</sub>} | {z<sub>1</sub>, z<sub>2</sub>} | valid |

![Antwort](./ESA1_Loesung4b.png)

c)

![Antwort](./ESA1_Loesung4c.png)
