## Aufgabe 1 – Relationen

```
Welche der Eigenschaften linkseindeutig, rechtseindeutig, linkstotal, rechtstotal, reflexiv, irreflexiv, symmetrisch, antisymmetrisch bzw. transitiv haben die folgenden Relationen?

Dabei ist ℤ die Menge der ganzen Zahlen, ℕ die Menge der natürlichen Zahlen ohne die Null und ℝ die Menge der reellen Zahlen.

Begründen Sie Ihre Entscheidungen für oder gegen diese Eigenschaften jeweils.

a) 𝐑 = {(𝑥, 𝑦) ∈ ℝ × ℝ | 𝑥 = |𝑦|}
b) 𝐑 = {(𝑥, 𝑦) ∈ ℝ × ℝ | 𝑥 ∙ 𝑦 = 0}
c) 𝐑 = {(𝑥, 𝑦) ∈ ℤ × ℤ | 𝑥 = 2𝑦}
```

# a) 𝐑 = {(𝑥, 𝑦) ∈ ℝ × ℝ | 𝑥 = |𝑦|}

|Aussage            |w|f|Begründung|
|-                  |-|-|-|
|linkseindeutig     |X| ||
|rechtseindeutig    | |X|positiver und negativer y-Wert stehen in Relation zum gleichen linken Element|
|linkstotal         | |X|es gibt keine Relationen (x,y) für x < 0|
|rechtstotal        |X| |
|reflexiv           | |X|(x,x) kein ∈ 𝐑 für x < 0 und weil nicht linkstotal|
|irreflexiv         | |X|weil (x,x) ∈ 𝐑 für x >= 0|
|symmetrisch        | |X|positive x stehen mit negativen y in Relation, aber negative y stehen mit keinem Element in Relation|
|antisymmetrisch    |X| |alle positiven Werte (und Null) stehen mit sich selbst in Relation|
|transitiv          |X| |wenn (a,b) ∈ 𝐑 und (b,c) ∈ 𝐑 dann ist b positiv oder 0 und somit a = b, und dies bedeutet logischerweise das (a,c) ∈ 𝐑|


# b) 𝐑 = {(𝑥, 𝑦) ∈ ℝ × ℝ | 𝑥 ∙ 𝑦 = 0}
|Aussage            |w|f|Begründung|
|-                  |-|-|-|
|linkseindeutig     | |X|jedes Element von links steht mit der 0 rechts in Relation|
|rechtseindeutig    | |X|jedes Element von rechts steht mit der 0 links in Relation|
|linkstotal         |X| |jedes Element links steht mit der 0 rechts in Relation|
|rechtstotal        |X| |jedes Element rechts steht mit der 0 links in Relation|
|reflexiv           | |X|es gibt keine Relationen (x,x) für x != 0
|symmetrisch        |X| |bei Multiplikation können die Faktoren beliebig getauscht werden|
|antisymmetrisch    | |X|Bsp. (5,0) ∈ 𝐑 und (0,5) ∈ 𝐑, 5 != 0
|transitiv          | |X|(a,0) ∈ 𝐑 und (0,b) ∈ 𝐑, dann müsste (a,b) ∈ 𝐑, dies gilt aber nur für a = 0 oder b = 0, für alle anderen Werte gilt dies nicht

# b) 𝐑 = {(𝑥, 𝑦) ∈ ℤ × ℤ | 𝑥 = 2𝑦}
|Aussage            |w|f|Begründung|
|-                  |-|-|-|
|linkseindeutig     | |X||
|rechtseindeutig    | |X||
|linkstotal         | |X|ungerade y stehen mit keinem Element von rechts in Relation|
|rechtstotal        |X| ||
|reflexiv           | |X|Bsp. 5 != 2 * 5|
|symmetrisch        | |X|Bsp. (2,1) ∈ 𝐑 aber nicht (1,2) ∈ 𝐑|
|antisymmetrisch    |X| |(a,b) ∈ 𝐑 und (b,a) ∈ 𝐑 gilt nur für a = b = 0|
|transitiv          | |X|Bsp. (4,2) ∈ 𝐑 und (2,1) ∈ 𝐑 aber nicht (4,1) ∈ 𝐑|

w = Aussage wahr  
f = Aussage falsch

##### Notizen

* linkseindeutig: keine zwei Elemente von links stehen mit dem selben Element von rechts in Relation (injektiv oder eineindeutig)
* linkstotal: jedes linke Element steht mit mind. 1 Element von rechts in Relation (sujektiv)
* injektiv + surjektiv = bijektiv
* reflexiv: jedes Element aus A steht mit sich selbst in Relation
* irreflexiv: kein Element aus A steht mit sich selbst in Relation
* symmetrisch: (a,b) ∈ 𝐑 dann (b,a) ∈ 𝐑
* asymmetrisch: (a,b) ∈ 𝐑 dann nicht (b,a) ∈ 𝐑
* antisymmetrisch: wenn (a,b) ∈ 𝐑 und (b,a) ∈ 𝐑 dann a = b
* transitiv: wenn (a,b) ∈ 𝐑 und (b,c) ∈ 𝐑, dann ist auch (a,c) ∈ 𝐑

---

## Aufgabe 2 – Äquivalenzrelation

```
Betrachten Sie die folgende Relation:
    𝐑 = {(𝑥, 𝑦) ∈ ℤ × ℤ | 3 teilt (2𝑥 + 𝑦)}

Handelt es sich bei 𝐑 um eine Äquivalenzrelation? Beweisen Sie Ihre Aussage. Falls es sich um eine Äquivalenzrelation handelt, geben Sie ihre Äquivalenzklassen an.
```
##### Antwort
* Reflexiv
  * 

##### Notizen
* Äquivalenzrelation heißt die Relation is reflexiv, symmetrisch und transitiv

---

## Aufgabe 3 -- Hasse-Diagramm

```
Zeichnen Sie das Hasse-Diagramm der Ordnungsrelation "ist Teiler von" für folgende Teilmenge der natürlichen Zahlen: 

{ 3, 4, 5, 8, 9, 12, 15, 24, 36, 60, 72 }
```

##### Antwort

![Antwort](/Zeichnung.png)