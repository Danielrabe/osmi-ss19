#### Name: Rabe | Vorname: Daniel | Martikelnummer: 2000049  

----

#### Aufgabe 1
```
function aufgabe1(n) {
    let z = 1;
    for (let i = 1; i <= n; i++) {
        let m = z;
        for (let j = 1; j <= m; j++) {
            z = z + 1;
        }
    }
    return z;
}

console.log(aufgabe1(3))
```
----
##### Lösungen:
a) 
|z|i|m|j|
|-|-|-|-|
|1| | | |
|1|1|1|1|
|2|2|2|1|
|3| | |2|
|4|3|4|1|
|5| | |2|
|6| | |3|
|7| | |4|
|8| | | |

Ausgabe: 8  

b) 1  (Anzahl der Parameter)  

c) Zeile 5, also das Ändern der Variable z  

d) (n² + n) / 2 (Gaußsche Summenformel)  
